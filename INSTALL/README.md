# Instalación

## Maquina Virtual

Video de ejemplo en YouTube:

https://www.youtube.com/watch?v=GEx046EHphI

Descarga:  Ubuntu
https://ubuntu.com/download/desktop

Descarga:  Manjaro
https://manjaro.org/download/


## Telegram.

- Ubuntu - Debian
~~~
sudo apt-get update
sudo apt-get install telegram-desktop

~~~

- Arch, Manjaro...

~~~
sudo pacman -Sy
sudo pacman -S telegram-desktop
~~~


## kleopatra.
- Ubuntu - Debian
~~~
sudo apt-get update
sudo apt-get install kleopatra

~~~

- Arch, Manjaro...

~~~
sudo pacman -Sy
sudo pacman -S kleopatra
~~~

## git  (opcional pero recomendado).

- Ubuntu - Debian
~~~
sudo apt-get update
sudo apt-get install git 

~~~

- Arch, Manjaro...

~~~
sudo pacman -Sy
sudo pacman -S git 
~~~


## Repositorio git

- Clonado
~~~
git clone https://framagit.org/nukebull/crytptodummies.git
~~~
- Actualización
~~~
cd crytptodummies
git pull origin main
~~~

## Correo Protonmail . com
- Crearse una cuenta fake para la practica, son gratuitas
- https://protonmail.com/


## Tener los hash SHA3 (opcional)
Instalacion de funciones hash SHA3

- Ubuntu Denian
~~~
sudo apt-get update
sudo apt-get install libdigest-sha3-perl
~~~
- Arch y derivadas
~~~
sudo pacman -Sy
sudo pacman -S sha3sum
~~~