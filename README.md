# Taller: CryptoDummies


## Conceptos Básicos.
La idea del taller es aprender conceptos básicos de criptografía, su uso y aplicación.

No entraremos de lleno en la matemática necesaria para su plena comprensión, tan solo , si fuera necesarío, pequeños apuntes para aclarar algunos conceptos, con un nivel similar al de un estudiante de secundaría.

Le daremos mayor importancia, al cifrado de clave pública y privada y como este ha cambiado el mundo.

## Material necesario para el taller. 
- Conexión a internet.
- Tener Telegram.
- Tener instalado un navegador (Firefox, Chromiun...).
- Cuenta fake o real de protonmail.
- Instalar el complemento Mailvelope para firefox o chromiun.
- PC con sistema operativo linux instalado o una maquina virtual con Linux.
- Micrófono y auriculares para aquellos que quieran participar mediante jitsi (webcam si se quiere).
- No tener wassa, facebug, gogle shit, ifone o windos... (Te hace mejor persona, pero no es necesarío para el taller).
- Se pondrá un repositorio git con todo lo necesarío. https://framagit.org/nukebull/cryptodummies
- Quien necesite ayuda para instalar algo, tenga una duda, puede contactar conmigo por telegram. @Nuke_Bull    
- Canal para el taller. https://t.me/joinchat/59FjkkuqOvljNjc0 

## Parte Teórica.
- Cifrar-Codificar, diferencia entre ambos.
- Diferentes tipos de cifrados y clasificación.
- Cifrado simétrico y asimétrico.
- Funciones Hash.

## Software.
- GnuPG - Kleopatra.
- Navegador Web.
- Terminal de Linux.

      
## Práctica.

- Simularemos protocolos para entender como funciona el mundo gracias a la criptografía. 
- Analizaremos los resultados obtenidos.

#### Games:
- Crear claves privada y pública
- Verificación de archivos.
- email/Protonmail/Mailvelope.
- Blockchain - Bitcoin.
- Red Tor.
