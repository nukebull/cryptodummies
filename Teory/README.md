### Codificación.

Se entiende por codificación en el contexto de la Ingeniería, al proceso de conversión de un sistema de datos de origen a otro sistema de datos de destino. De ello se desprende como corolario que la información contenida en esos datos resultantes deberá ser equivalente a la información de origen. Un modo sencillo de entender el concepto es aplicar el paradigma de la traducción entre idiomas en el ejemplo siguiente: home = hogar. Podemos entender que hemos cambiado una información de un sistema (inglés) a otro sistema (español), y que esencialmente la información sigue siendo la misma.


### Cifrado:
En criptografía, el cifrado es un procedimiento que utiliza un algoritmo de cifrado con cierta clave (clave de cifrado) para transformar un mensaje, sin atender a su estructura lingüística o significado, de tal forma que sea incomprensible o, al menos, difícil de comprender a toda persona que no tenga la clave secreta (clave de descifrado) del algoritmo. Las claves de cifrado y de descifrado pueden ser iguales (criptografía simétrica), distintas (criptografía asimétrica) o de ambos tipos (criptografía híbrida).

### XOR
La puerta XOR, compuerta XOR u OR exclusiva es una puerta lógica digital que implementa el o exclusivo; es decir, una salida verdadera (1/HIGH) resulta si una, y solo una de las entradas a la puerta es verdadera. Si ambas entradas son falsas (0/LOW) o ambas son verdaderas, resulta en una salida falsa. La XOR representa la función de la desigualdad, es decir, la salida es verdadera si las entradas no son iguales, de otro modo el resultado es falso. Una manera de recordar XOR es "uno o el otro, pero no ambos". 

### Cifrado Simétrico

En esquemas de clave simétrica, las claves de cifrado y descifrado son las mismas. Las partes comunicantes deben tener la misma clave para lograr una comunicación segura. Un ejemplo de una clave simétrica es la máquina Enigma del ejército alemán. Había configuraciones clave para cada día. Cuando los Aliados descubrieron cómo funcionaba la máquina, pudieron descifrar la información codificada dentro de los mensajes tan pronto como pudieron descubrir la clave de cifrado para las transmisiones de un día determinado.

#### Cifrador de flujo.
El funcionamiento del generador de flujo de claves en A5/1, un cifrado de flujo basado en LFSR que se utiliza para cifrar conversaciones de teléfonos móviles.

Un cifrado de flujo es un cifrado de clave simétrica en el que los dígitos de texto plano se combinan con un flujo de dígitos de cifrado pseudoaleatorio (flujo de claves). En un cifrado de flujo, cada dígito de texto sin formato se cifra uno a la vez con el dígito correspondiente del flujo de claves, para dar un dígito del flujo de texto cifrado. Dado que el cifrado de cada dígito depende del estado actual del cifrado, también se conoce como cifrado de estado. En la práctica, un dígito es típicamente un bit y la operación de combinación es un exclusivo-o (XOR). 

#### Cifrado por bloques.

En criptografía, una unidad de cifrado por bloques (en inglés, block cipher) es una unidad de cifrado de clave simétrica que opera en grupos de bits de longitud fija, llamados bloques, aplicándoles una transformación invariante. Cuando realiza cifrado, una unidad de cifrado por bloques toma un bloque de texto plano o claro como entrada y produce un bloque de igual tamaño de texto cifrado. La transformación exacta es controlada utilizando una segunda entrada — la clave secreta. El descifrado es similar: se ingresan bloques de texto cifrado y se producen bloques de texto plano. 

### Cifrado Asimétrico

En los esquemas de cifrado de clave pública, la clave de cifrado se publica para que cualquiera pueda usar y cifrar mensajes. Sin embargo, solo la parte receptora tiene acceso a la clave de descifrado que permite leer los mensajes4​. El cifrado de clave pública se describió por primera vez en un documento secreto en 19735​; antes de eso, todos los esquemas de cifrado eran de clave simétrica (también llamada clave privada)6​ . Aunque se publicó posteriormente, el trabajo de Diffie y Hellman, fue publicado en una revista con un gran número de lectores, y el valor de la metodología se describió explícitamente7​ y el método se conoció como el intercambio de claves Diffie Hellman.Una aplicación de cifrado de clave pública disponible públicamente llamada Pretty Good Privacy (PGP) fue escrita en 1991 por Phil Zimmermann y distribuida gratuitamente con el código fuente. PGP fue comprado por Symantec en 2010 y se actualiza regularmente8​ . A este tipo de cifrado también se le llama criptografía de clave pública o PKE (del inglés Public-Key Encryption). 

La utilización de un sistema simétrico o asimétrico depende de las tareas a cumplir. La criptografía asimétrica presenta dos ventajas principales: suprime el problema de transmisión segura de la clave y permite la firma electrónica. No reemplaza sin embargo los sistemas simétricos, ya que los tiempos de cálculo son evidentemente más cortos con los sistemas simétricos que con los asimétricos. 

### Función hash

Una función resumen, en inglés hash function, también conocida con el híbrido función hash, convierte uno o varios elementos de entrada a una función en otro elemento.​​​ También se las conoce como función extracto, del inglés digest function, función de extractado y por el híbrido función digest.

Una función hash H es una función computable mediante un algoritmo tal que:
H : U → M 
x → h ( x )
La función hash tiene como entrada un conjunto de elementos, que suelen ser cadenas, y los convierte en un rango de salida finito, normalmente cadenas de longitud fija. Es decir, la función actúa como una proyección del conjunto U sobre el conjunto M. 


#### Bibliografía: 
Wikipedia: https://es.wikipedia.org
