# Blockchaim-Bitcoins


Se verá partes del funcionamiento del protocolo bitcoin, haremos una simulación idealizada de minado.

#### Hash de verificación

###### minar.sh
md5:
e3ff1a6e628aacb90e777df754bfaeaa
sha256:
b0cd5fe76cb0274025a3d7c8dad21445034af2bb3667016d956f7351c84f1b8e
##### minar1000.sh
md5:
f5f5d9c626d176272c457e7fc93f54d2
sha256:
96b9b801aa6ca1454f60a8ddf1b15cefcb2d96186598d8255ba999ef00492118

#### Pactica de Minado de hackCoins

El archivo minar. sh contiene un script que en el que buscaremos un hash por fuerza bruta (sha256).

Dar permisos de ejecución al script
~~~
chmod +x minar.sh
~~~
Ejecutar script minar. sh
~~~
./minar.sh arg1 arg2
~~~
Ejecutar y ver cuanto tarda
~~~
time ./minar.sh arg1 arg2
~~~


#### Ejemplos

- Usaremos: Texto conocido + número desconocido = hash conocido
- La practica es buscar que número es el que satisface el hash conocido.

#### 100
- arg1: hola
- arg2: 863edfe440a8d7b07f89801b562587386a425a90a4298ca82de91c2b11ca05e8
- arg2: ea99a2b6e06f8702e3cb412fe47bf5eed926fdca768a4b0e97b5937d961f31fd

####1000
- arg1: hola 
- arg2: e24dfd9703629d654d2b2cb208d7d6cbf5677c0e1baf408abc8a017f32eb5397
- arg2: 7d549bbbc5f3eb42417c112a23e45164768ff30626363a895c5354068e982878

