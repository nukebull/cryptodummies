#!/bin/bash

## Entraremos dos argumentos
## arg 1 -> La parte conocida
## arg2 -> El hash final para buscar los numeros secretos

for i in {0..9}
do
    for j in {0..9}
    do
        hash=$( echo "$1$i$j" | sha256sum )
        echo "Itección $i$j"
        echo $hash  
        mhash=${hash::-2}

        if [ $mhash == "$2" ]
        then
            echo "El número es: $i$j"
            exit
        else
            echo "Hemos Fallado"
            echo " "

        fi
    done
done   