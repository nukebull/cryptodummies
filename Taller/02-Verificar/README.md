# Verificación-Integridad

## Practica.
1. Comprobaremos la integridad de una descarga
2. Crearemos un hash para la verificacion de nuestro archivo.

### Bash
En la carpeta 04-Bitcoins, tenemos dos archivos minar.sh y minar1000.sh debemos buscar los hash publicados y verificar que son correctos.

Para hacerlo debemos usar la Terminal de Bash
~~~
md5sum minar.sh
sha256sum minar.sh
md5sum minar1000.sh
sha256sum minar1000.sh
~~~

#### Hash disponibles

- Con el sistema original

md5sum, sha1sum, sha256sum, sha512sum, sha224sum, sha384sum   

- Despues de instalar shs3sum
sha3-224sum, sha3-256sum, sha3-384sum, sha3-512sum, sha3sum.

### GnuPG

- Tor Browser

~~~
gpg --auto-key-locate nodefault,wkd --locate-keys torbrowser@torproject.org

gpg -a --output ./tor.keyring.asc --export 0xEF6E286DDA85EA2A4BA7DE684E2C6E8793298290

gpgv --keyring ./tor.keyring tor-browser-linux64-10.5.6_en-US.tar.xz.asc tor-browser-linux64-10.5.6_en-US.tar.xz
~~~